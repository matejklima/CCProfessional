import json

import simplejson as simplejson
from django import template
from django.core.serializers import serialize
from django.db.models import QuerySet

register = template.Library()


@register.filter
def get_item(list, item):
    return list[item]

@register.filter
def mult(value, arg):
    "Multiplies the arg and the value"
    return float(value) * float(arg)

@register.filter
def jsonify(object):
    # print("jsonify: " + str(object))
    if isinstance(object, QuerySet):
        return serialize('json', object)
    # print("in json: " + json.dumps(object))
    return json.dumps(object)

@register.filter
def pass_request_params(req_params):
    print("pass params: " + str(object))
    res_params = ""
    for key in req_params:
        res_params += key
        res_params += "="
        res_params += req_params[key]
        res_params += "&"
    res_params=res_params[:-1]
    print("resulted params: " + res_params)
    return res_params