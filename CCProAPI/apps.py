from django.apps import AppConfig


class CcproapiConfig(AppConfig):
    name = 'CCProAPI'
