# Create your views here.


from django.shortcuts import render, render_to_response
from django.http import HttpResponse, JsonResponse
from django.template import Context, loader
import json, requests
import os

currencies = requests.get('https://api.coinmarketcap.com/v1/ticker/')
# currencies = open(os.path.join(os.path.dirname(os.path.dirname(__file__)),'CCProPortfolio/static/CCProPortfolio/coinmarketcap.json')).read()
rates = requests.get('http://apilayer.net/api/live?access_key=cd1747e1d9ea0ba51a5771aea42cc7d4&currencies=EUR,CZK&source=USD&format=1')
# rates = open(os.path.join(os.path.dirname(os.path.dirname(__file__)),'CCProPortfolio/static/CCProPortfolio/apilayer.json')).read()

def select_currencies(request):
    template = loader.get_template('CCProApp/select_currencies.html')
    currenciesJson = json.loads(currencies.text)
    # currenciesJson = json.loads(currencies)
    context = {
        'cryptocurrencies' : currenciesJson,
    }
    return HttpResponse(template.render(context, request))


def select_currencies_item(request):
    formId = int(request.GET['formId']) + 1
    template = loader.get_template('CCProApp/select_currencies_item.html')
    currenciesJson = json.loads(currencies.text)
    # currenciesJson = json.loads(currencies)
    context = {
        'cryptocurrencies' : currenciesJson,
        'formId' : formId,
    }
    return HttpResponse(template.render(context, request))


def portfolio(request):
    template = loader.get_template('CCProApp/portfolio_default.html')
    currenciesJson = json.loads(currencies.text)
    # currenciesJson = json.loads(currencies)
    context = {
        'cryptocurrencies' : currenciesJson,
    }
    return HttpResponse(template.render(context, request))


def selection_submit(request):
    try:
        selected_choice = request.GET['currency']
        choices = request.GET
    except (KeyError):
        # Redisplay the question voting form.
        return select_currencies(request)
    else:
        print(selected_choice)
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return portfolio(request)
