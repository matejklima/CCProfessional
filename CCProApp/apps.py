from django.apps import AppConfig


class CcproappConfig(AppConfig):
    name = 'CCProApp'
